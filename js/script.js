$("#result_list thead tr").prepend("<th>attend to pool");
$("#result_list tbody tr").each(function(index) {
  var requestId = $(this)
    .find(".field-request_id")
    .text();
  var requestUrl = $(this)
    .find(".field-request_id")
    .find("a")
    .attr("href");
  var requestSlug = $(this)
    .find(".field-slug")
    .text();
  console.log({ requestSlug });
  var requestFullName = $(this)
    .find(".field-full_name")
    .text();
  var requestBudget = $(this)
    .find(".field-budget")
    .text();
  var requestPerHour = $(this)
    .find(".field-per_hour")
    .text();
  var requestEmail = $(this)
    .find(".field-email")
    .text();
  var requestSubjects = $(this)
    .find(".field-request_subjects")
    .text();
  if (!requestUrl) {
  }
  
  $(this).prepend(
    `<td><button type="button"` +
      `data-request-id="${requestId}" ` +
      `data-request-url="${requestUrl}" ` +
      `data-request-slug="${requestSlug}" ` +
      `data-request-email="${requestEmail}" ` +
      `data-request-fullname="${requestFullName}" ` +
      `data-request-budget="${requestBudget}" ` +
      `data-request-perhour="${requestPerHour}" ` +
      `data-request-subjects="${requestSubjects}" ` +
      'class="btn btn-primary btn-sm clientRequestJs" data-toggle="modal" data-target="#clientRequestModal">' +
      "Detail" +
      "</button ></td>"
  );
});

$(".clientRequestJs").on("click", function() {
  let requestId = $(this).data("requestId");
  let requestUrl = $(this).data("requestUrl");
  let requestSlug = $(this).data("requestSlug");
  let clientName = $(this).data("requestFullname");
  let clientEmail = $(this).data("requestEmail");
  let requestSubjects = $(this).data("requestSubjects");
  let totalBudget = $(this).data("requestBudget");
  let hourlyBudget = $(this).data("requestPerhour");
  let obj = {
    requestId,
    requestUrl,
    requestSlug,
    clientName,
    clientEmail,
    requestSubjects,
    totalBudget,
    hourlyBudget
  };
  
  console.log(obj);
  window.unMountSalesModal();
  window.renderSalesModal(obj);
});