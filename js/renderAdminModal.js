$(".bookingJs").on("click", function () {
  let requestId = $(this).parents("tr").find(".field-request_id a").text();
  let clientEmail = $(this).parents("tr").find(".field-email").text();
  let clientName = $(this).parents("tr").find(".field-full_name").text();
  let tutorSelectedEmail = $(this)
    .parents("tr")
    .find(".field-tutor_selected")
    .text();
  let remarks = $(this).parents("tr").find(".field-remarks p").text();
  let budget = $(this).parents("tr").find(".field-budget").text();
  let requestUrl = $(this).data("requestUrl");
  let requestSlug = $(this).data("requestSlug");
  let obj = {
    requestId,
    requestUrl,
    requestSlug,
    clientName,
    clientEmail,
    tutorSelectedEmail,
    remarks,
    budget,
  };
  console.log(obj);
  window.unMountBookingModal();
  window.renderBookingModal(obj);
});

$(".clientRequestJs").on("click", function () {
  let requestId = $(this).parents("tr").find(".field-request_id a").text();
  let clientEmail = $(this).parents("tr").find(".field-email").text();
  let clientName = $(this).parents("tr").find(".field-full_name").text();
  let totalBudget = $(this).parents("tr").find(".field-budgets a").text();
  let hourlyBudget = $(this).parents("tr").find(".field-per_hour").text();
  let requestSubjects = $(this)
    .parents("tr")
    .find(".field-request_subjects")
    .text();

  let requestSlug = $(this).data("requestSlug");
  let requestUrl = $(this).data("requestUrl");
  let isParentRequest = $(this).data("requestIsparent");
  let selectCurrency = $(this)
    .parents("tr")
    .find(".field-request_currency")
    .text();
  let convertedTotalBudget = $(this)
    .parents("tr")
    .find(".field-request_converted_budget")
    .text();
  // let convertedHourlyBudget = $(this)
  //   .parents("tr")
  //   .find(".field-request_converted_hourly")
  //   .text();

  let obj = {
    requestId,
    isParentRequest: isParentRequest === "True",
    requestUrl,
    requestSlug,
    clientName,
    clientEmail,
    requestSubjects,
    totalBudget,
    selectCurrency,
    hourlyBudget,
    convertedTotalBudget,
    // convertedHourlyBudget,
  };
  window.unMountSalesModal();
  window.renderSalesModal(obj, {
    baseUrl: window.ADMIN_MODAL_BASE_URL,
    userId: window.USER_ID,
  });
});
